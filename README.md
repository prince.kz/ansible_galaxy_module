# Ansible Galaxy Module

Ansible_galaxy_module is a project designed to work idempotently with Ansible Galaxy roles and collections on destination hosts. The primary goal is to provide a tool for idempotent operations on remote hosts using modules such as docker-compose, but the module can be adapted for various use cases.

## Features

- Idempotent installation of Ansible Galaxy roles.
- Logging in to Ansible Galaxy and storing the authentication token in the environment.
- Idempotent role publishing on Ansible Galaxy.
- Retrieving information about installed roles.
- Idempotent removal of Ansible roles.

## Getting Started

### Installation

To use the Ansible_galaxy_module, you need to have Ansible installed on your local machine. Additionally, ensure that Ansible Galaxy is installed.

You can install the module using the following steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/prince.kz/ansible_galaxy_module.git
    ```

2. Change to the project directory:

    ```bash
    cd ansible_galaxy_module
    ```

3. Run the module:

    ```bash
    python3 ansible_galaxy_module.py --help
    ```

### Usage

The module provides several functions to manage Ansible Galaxy roles:

- `ansible_galaxy_install(role_name)`: Installs an Ansible role if it is not already installed.
- `ansible_galaxy_login()`: Logs in to Ansible Galaxy, storing the authentication token in the environment.
- `ansible_galaxy_publish()`: Publishes the current role to Ansible Galaxy if it is not already published.
- `ansible_galaxy_info(role_name)`: Retrieves information about an installed Ansible role.
- `ansible_galaxy_remove(role_name)`: Removes an installed Ansible role.

Each function is designed to be idempotent, ensuring that operations are only performed when necessary.

### Example Playbook

```yaml
---
- name: Manage Ansible Galaxy Roles
  hosts: localhost
  gather_facts: false

  tasks:
    - name: Install Ansible Role
      command: python3 ansible_galaxy_module.py ansible_galaxy_install username.role
      register: install_result
      changed_when: "'changed' in install_result.stdout"

    - name: Login to Ansible Galaxy
      command: python3 ansible_galaxy_module.py ansible_galaxy_login
      register: login_result
      changed_when: "'changed' in login_result.stdout"

    - name: Publish Ansible Role
      command: python3 ansible_galaxy_module.py ansible_galaxy_publish
      register: publish_result
      changed_when: "'changed' in publish_result.stdout"

    - name: Get Ansible Role Info
      command: python3 ansible_galaxy_module.py ansible_galaxy_info username.role
      register: info_result
      changed_when: "'changed' in info_result.stdout"

    - name: Remove Ansible Role
      command: python3 ansible_galaxy_module.py ansible_galaxy_remove username.role
      register: remove_result
      changed_when: "'changed' in remove_result.stdout"

    - name: Display Results
      debug:
        var: item.stdout
      loop:
        - "{{ install_result }}"
        - "{{ login_result }}"
        - "{{ publish_result }}"
        - "{{ info_result }}"
        - "{{ remove_result }}"
