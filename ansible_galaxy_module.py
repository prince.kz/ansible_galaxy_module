#!/usr/bin/python3
#https://gitlab.com/prince.kz/ansible_galaxy_module.git
#Created by Ussin Aibek aka P.B. prince.kz located on server gmail.com
import os
import subprocess
import json


def run_command(command):
    """
    Функция для выполнения команд в консоли.
    """
    try:
        result = subprocess.run(command, capture_output=True, text=True, check=True)
        return result.stdout.strip()
    except subprocess.CalledProcessError as e:
        return e.stderr.strip()


def ansible_galaxy_install(role_name):
    """
    Устанавливает Ansible-роль с использованием ansible-galaxy.
    """
    installed_roles = json.loads(run_command(["ansible-galaxy", "list", "--json"]))

    if any(role["name"] == role_name for role in installed_roles):
        return "ok", f"The role '{role_name}' is already installed."

    run_command(["ansible-galaxy", "install", role_name])
    return "changed", f"The role '{role_name}' has been installed successfully."


def ansible_galaxy_login():
    """
    Входит в учетную запись Ansible Galaxy.
    """
    existing_token = os.environ.get("ANSIBLE_GALAXY_TOKEN")

    if existing_token:
        return "ok", "Token already exists in the environment."

    username = os.environ.get("ANSIBLE_GALAXY_USERNAME")
    password = os.environ.get("ANSIBLE_GALAXY_PASSWORD")

    if username and password:
        command = ["ansible-galaxy", "login", "--username", username, "--password", password]
        result = run_command(command)

        if "Logged in as" in result:
            fake_token = result.split(":")[1].strip()
            os.environ["ANSIBLE_GALAXY_TOKEN"] = fake_token
            return "changed", "Successfully logged in to Ansible Galaxy."

    return "fail", "Failed to log in to Ansible Galaxy."


def ansible_galaxy_publish():
    """
    Публикует роль на Ansible Galaxy.
    """
    existing_token = os.environ.get("ANSIBLE_GALAXY_TOKEN")

    if not existing_token:
        return "fail", "Not logged in. Please run ansible_galaxy_login first."

    result = run_command(["ansible-galaxy", "publish", "--json"])

    try:
        parsed_result = json.loads(result)

        if parsed_result["published"]:
            return "ok", "The role has already been published."
        else:
            run_command(["ansible-galaxy", "publish"])
            return "changed", "The role has been published successfully."

    except json.JSONDecodeError:
        return "fail", "Failed to parse the output of ansible-galaxy publish."


def ansible_galaxy_info(role_name):
    """
    Возвращает информацию о роли из Ansible Galaxy.
    """
    existing_token = os.environ.get("ANSIBLE_GALAXY_TOKEN")

    if not existing_token:
        return "fail", "Not logged in. Please run ansible_galaxy_login first."

    installed_roles = json.loads(run_command(["ansible-galaxy", "list", "--json"]))

    if any(role["name"] == role_name for role in installed_roles):
        result = run_command(["ansible-galaxy", "info", role_name])
        return "ok", result

    return "fail", f"The role '{role_name}' is not installed."


def ansible_galaxy_remove(role_name):
    """
    Удаляет Ansible-роль с использованием ansible-galaxy.
    """
    existing_token = os.environ.get("ANSIBLE_GALAXY_TOKEN")

    if not existing_token:
        return "fail", "Not logged in. Please run ansible_galaxy_login first."

    installed_roles = json.loads(run_command(["ansible-galaxy", "list", "--json"]))

    if any(role["name"] == role_name for role in installed_roles):
        run_command(["ansible-galaxy", "remove", role_name])
        return "changed", f"The role '{role_name}' has been removed."

    return "ok", f"The role '{role_name}' is not installed."

# Пример вызова функции для входа в учетную запись Ansible Galaxy
# result, message = ansible_galaxy_login()
# print(f"Result: {result}, Message: {message}")
